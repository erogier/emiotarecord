//
//  MoveCell.swift
//  EmiotaRecordMove
//
//  Created by Eddy Rogier on 24/05/2016.
//  Copyright © 2016 EddyRogier. All rights reserved.
//

import UIKit

class MoveCell: UITableViewCell {

    @IBOutlet weak var moveImage: UIImageView!
    @IBOutlet weak var moveTitle: UILabel!
    @IBOutlet weak var moveUnitMinOrStep: UILabel!
    @IBOutlet weak var moveObjectif: UILabel!
    @IBOutlet weak var moveUnitObjectitif: UILabel!
    @IBOutlet weak var moveImageDone: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
