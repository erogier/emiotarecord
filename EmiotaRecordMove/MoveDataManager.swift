//
//  MoveDataManager.swift
//  EmiotaRecordMove
//
//  Created by Eddy Rogier on 24/05/2016.
//  Copyright © 2016 EddyRogier. All rights reserved.
//

import UIKit
/** Var Global : Instance de la classe MoveDataManager */
var moveMgr: MoveDataManager = MoveDataManager()

/** une struct de "movement [ title, isMin, Objectif ]" */
struct moveStruct {
    var image:UIImage = UIImage(named: "default")!
    var title = "default title"
    var isSecond = true
    var objectifCurrent = 0
    var objectif = 0 //\/  second or Step if bool isSecond == False
    var arrDataGyro:[[Double]] = []
    var arrDataAcce:[[Double]] = []
}


class MoveDataManager: NSObject {
    
    /** compteur */
    var counter = 0
    
    /** Le Tableau des mouvements de la classe MoveDataManager*/
    var movesArr = [
            //\/  IMPORTANT with the image
            /*
                default.png =  UIImage.init(named: "default")!          // GOOD NO ERROR !
                default.jpg =  UIImage.init(named: "default")!          // BAD     ERROR because .jpg !
                so 
                default.jpg =  UIImage.init(named: ("default.jpg"))!    // GOOD NO ERROR encapsulate (.jpg) !
            */
        
    moveStruct(image: UIImage.init(named: ("move01immobile.jpg"))!, title: "Rester Immobile", isSecond: true, objectifCurrent: 0, objectif: 10, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move02assis.jpg"))!, title: "M'asseoir", isSecond: false, objectifCurrent: 0, objectif: 3, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move03seLever.jpg"))!, title: "Me lever", isSecond: false, objectifCurrent: 0, objectif: 3, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move04Marcher.jpg"))!, title: "Marche", isSecond: true, objectifCurrent: 0, objectif: 12, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move05monterEscalier.jpg"))!, title: "monter les escaliers", isSecond: true, objectifCurrent: 0, objectif: 5, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move06descendreEscalier.jpg"))!, title: "Descendre les escaliers", isSecond: true, objectifCurrent: 0, objectif: 5, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move07courir.jpg"))!, title: "Courir", isSecond: true, objectifCurrent: 0, objectif: 10, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move08sauter.jpg"))!, title: "Sauter", isSecond: false, objectifCurrent: 0, objectif: 4, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move09accroupi.jpg"))!, title: "S'accroupir", isSecond: false, objectifCurrent: 0, objectif: 3, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move10pompe.jpg"))!, title: "Faire des pompes", isSecond: true, objectifCurrent: 0, objectif: 1, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move11abdo.jpg"))!, title: "Faire des abdominaux", isSecond: true, objectifCurrent: 0, objectif: 15, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move12allonger.jpg"))!, title: "M'allonger", isSecond: false, objectifCurrent: 0, objectif: 3, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move13rire.jpg"))!, title: "Rigoler", isSecond: true, objectifCurrent: 0, objectif: 8, arrDataGyro:[],arrDataAcce:[] ),
    moveStruct(image: UIImage.init(named: ("move14tousser.jpg"))!, title: "Tousser", isSecond: true, objectifCurrent: 0, objectif: 4, arrDataGyro:[],arrDataAcce:[] ),
    
        
        ]
    
    func addMoove(title:String, isMin:Bool, objectif:Int) {
        /*
         accelerometre
         gyroscope
         timer
         tableau pour stockeer les capteurs
         */
        
    }
    
    
}
