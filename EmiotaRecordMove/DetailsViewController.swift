//
//  DetailsViewController.swift
//  EmiotaRecordMove
//
//  Created by Eddy Rogier on 24/05/2016.
//  Copyright © 2016 EddyRogier. All rights reserved.
//https://github.com/vandadnp/iOS-8-Swift-Programming-Cookbook
//

import UIKit
import CoreMotion

class DetailsViewController: UIViewController {
    
    // MARK: - Reference of Element Storyboard
    @IBOutlet weak var dvcTitle:                UILabel!
    @IBOutlet weak var dvcImage:                UIImageView!
    
    @IBOutlet weak var dvcStepOrSecond:         UILabel!
    @IBOutlet weak var dvcTimerLocalForStep:    UILabel!
    @IBOutlet weak var buttonRecord:            UIButton!
    
    // MARK: - CONSTANTE AND VARIABLE
    
    var DVCindexPath:NSIndexPath? // index tableview
    var currentObjectif:Int         = 0 //\/ (~￣▽￣)~ objectif en cours
    var localTimer                  = 1
    var localTimerReset             = 1
    var localtimerInstance:NSTimer  = NSTimer()
    
    let sensor = CMMotionManager()
    let updateIntervalSensor:Double = 1 // One second update
    var isActiveSensorGyroscope:    Bool = false
    var isActiveSensorAccelerometer:Bool = false
    
    var arrAllDataGyro:[[Double]] = [] // Stock DataGyroscope From Closure
    var arrAllDataAcce:[[Double]] = [] // Stock DataAccelerom From Closure
    //\/ -------------------------
    
    override func viewDidLoad() {

        configurationStepOrSecond()
    }
    
    // MARK: -  Point Enter First
    func configurationStepOrSecond() {
        if moveMgr.movesArr[DVCindexPath!.row].isSecond {
            setViewForSecond()
            hydrateDataForSecond()
        } else {
            hydrateDataForStep()
        }
    }
    
    // MARK: - Button Action For SECOND Or STEP
    @IBAction func actionRecord(sender: AnyObject) {
        if moveMgr.movesArr[DVCindexPath!.row].isSecond {
            startRecordForSecond()
        } else {
            startRecordForStep()
        }
    }
    
    // MARK: - SECOND
    func setViewForSecond(){
        dvcTimerLocalForStep.hidden = true
    }
    
    func hydrateDataForSecond() {
        dvcTitle.text               = moveMgr.movesArr[DVCindexPath!.row].title
        dvcImage.image              = moveMgr.movesArr[DVCindexPath!.row].image
        currentObjectif             = moveMgr.movesArr[DVCindexPath!.row].objectifCurrent
        dvcStepOrSecond.text        = String(format: "Second : %d / %d",currentObjectif ,moveMgr.movesArr[DVCindexPath!.row].objectif)
        
        showResetForObjectifDone()
    }
    func startRecordForSecond(){
        if (currentObjectif != moveMgr.movesArr[DVCindexPath!.row].objectif) {
            hideButton()
            localtimerInstance = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(setTimerForSecond), userInfo: nil, repeats: true)
        } else {
            print("Objectif déja atteind")
        }
    }
    func setTimerForSecond(){
        if (currentObjectif != moveMgr.movesArr[DVCindexPath!.row].objectif) {
            //\/ Enregistrement continue
            currentObjectif += 1
            dvcStepOrSecond.text = String(format: "Second : %d / %d",currentObjectif ,moveMgr.movesArr[DVCindexPath!.row].objectif)
            
            //\/ Gyroscope & Accelrometer Record Section
            verifBeforeStartGyroscope()
            verifBeforeStartAccelerometer()
            //\/ -------------------------
            print("TIMER : \(currentObjectif)")
        } else {
            //\/ Enregistrement Finie, Objectif Atteind
            localtimerInstance.invalidate()
            moveMgr.movesArr[DVCindexPath!.row].objectifCurrent = currentObjectif //\/  update The Model MoveDataManager
            
            //\/  Gyroscope & accelerometer Record Section
            isActiveSensorGyroscope = false
            sensor.stopGyroUpdates()
            isActiveSensorAccelerometer = false
            sensor.stopAccelerometerUpdates()
            //\/ -------------------------
            
            //\/  update Model with Data Sensore
            moveMgr.movesArr[DVCindexPath!.row].arrDataGyro = arrAllDataGyro
            moveMgr.movesArr[DVCindexPath!.row].arrDataAcce = arrAllDataAcce
            
            showButton()
            buttonResetValue()
        }
    }
    
    // MARK: - STEP
    func hydrateDataForStep() {
        dvcTitle.text               = moveMgr.movesArr[DVCindexPath!.row].title
        dvcImage.image              = moveMgr.movesArr[DVCindexPath!.row].image
        currentObjectif             = moveMgr.movesArr[DVCindexPath!.row].objectifCurrent
        dvcStepOrSecond.text        = String(format: "Step : %d / %d",currentObjectif ,moveMgr.movesArr[DVCindexPath!.row].objectif)
        dvcTimerLocalForStep.text   = String(format: "Timer : %d second", localTimer)
        
        //showButton If Objectif termine
        showResetForObjectifDone()
    }
    func startRecordForStep() {
        if (currentObjectif != moveMgr.movesArr[DVCindexPath!.row].objectif) {
            hideButton()
            localtimerInstance = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(setTimerForStep), userInfo: nil, repeats: true)
        }
    }
    func setTimerForStep() {
        if (localTimer != 0) {
            localTimer -= 1
            dvcTimerLocalForStep.text = String(format: "Timer : %d second", localTimer)
            
            //\/  Gyroscope & Accelrometer Record Section
            verifBeforeStartGyroscope()
            verifBeforeStartAccelerometer()
            //\/ -------------------------
        } else {
            localtimerInstance.invalidate()
            localTimer = localTimerReset //reset le timer
            dvcTimerLocalForStep.text = String(format: "Timer : %d second", localTimer) // update timer on the view
            currentObjectif += 1 //update step objectif
            moveMgr.movesArr[DVCindexPath!.row].objectifCurrent = currentObjectif
            
            //\/ Gyroscope & accelerometer Record Section
            isActiveSensorGyroscope = false
            sensor.stopGyroUpdates()
            isActiveSensorAccelerometer = false
            sensor.stopAccelerometerUpdates()
            //\/ -------------------------
            
            //\/  update Model with Data Sensore
            moveMgr.movesArr[DVCindexPath!.row].arrDataGyro = arrAllDataGyro
            moveMgr.movesArr[DVCindexPath!.row].arrDataAcce = arrAllDataAcce
            
            dvcStepOrSecond.text    = String(format: "Step : %d / %d",currentObjectif ,moveMgr.movesArr[DVCindexPath!.row].objectif)
            showButton()
            
            if (moveMgr.movesArr[DVCindexPath!.row].objectifCurrent == moveMgr.movesArr[DVCindexPath!.row].objectif) {
                buttonResetValue()
            }
        }
    }
    
    
    
    // MARK: - Show / Hide button
    func showButton() {
        self.buttonRecord.hidden = false
        self.navigationItem.setHidesBackButton(false, animated: false)
    }
    func hideButton() {
        self.buttonRecord.hidden = true
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    func controleShowHideButton() {
        if (currentObjectif != moveMgr.movesArr[DVCindexPath!.row].objectif) {
            self.buttonRecord.hidden = false
        } else {
            self.buttonRecord.hidden = true
            buttonResetValue()
        }
    }
    
    // MARK: - Button Action Reset
    func buttonResetValue() {
        let button   = UIButton(type: UIButtonType.System) as UIButton
        button.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        button.backgroundColor = UIColor.init(white: 1.0, alpha: 0.9)
        button.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        button.titleLabel?.textAlignment = NSTextAlignment.Center
        button.setTitle("Enregistrement Terminé \n Cliquer pour reset et recommencer", forState: UIControlState.Normal)
        button.titleLabel?.font = UIFont(name: "...", size: 25)
        button.addTarget(self, action: #selector(buttonAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(button)
    }
    func buttonAction(sender:UIButton!) {
        if moveMgr.movesArr[DVCindexPath!.row].isSecond {
            print("Reset For Second")
            // TODO: - reset value
            
            //\/
            print("Model DataGyro : \(moveMgr.movesArr[DVCindexPath!.row].arrDataGyro)")
            print("Model DataAcce : \(moveMgr.movesArr[DVCindexPath!.row].arrDataAcce)")
            
        } else {
            print("Reset for step")
            
            print("Model DataGyro : \(moveMgr.movesArr[DVCindexPath!.row].arrDataGyro)")
            print("Model DataAcce : \(moveMgr.movesArr[DVCindexPath!.row].arrDataAcce)")
            // TODO: - reset value
        }
        // TODO: - Effectuer une mise a jour des value quand on appuie sur lebouton reset !!!!
    }
    func showResetForObjectifDone (){
        //\/ (~￣▽￣)~ si Objectif atteind..
        if (moveMgr.movesArr[DVCindexPath!.row].objectifCurrent == moveMgr.movesArr[DVCindexPath!.row].objectif) {
            buttonResetValue()
        }
    }
    
    // MARK: - Gyroscope Record Section
    func verifBeforeStartGyroscope(){
        if (isActiveSensorGyroscope == false) {
            //print("Activation du gyro")
            isActiveSensorGyroscope = true
            startRecordGyroscope()
        } else {
            //print("Activation du gyro déjà effectué")
        }
    }
    func startRecordGyroscope() {
        if sensor.gyroAvailable {
            //print(" gyro Available")
            sensor.gyroUpdateInterval = updateIntervalSensor
            let queue = NSOperationQueue.mainQueue()
            sensor.startGyroUpdatesToQueue(queue) {
                (data, error) in
                
                //\/ Life beginning of closure
                print("gyroscope || \nx = \(Double(data!.rotationRate.x))\ny = \(Double(data!.rotationRate.y))\nz = \(Double(data!.rotationRate.z)) \n")
                 self.arrAllDataGyro.append([
                        data!.rotationRate.x,
                        data!.rotationRate.y,
                        data!.rotationRate.z
                    ])
                // Death beginning of closure
            }
        } else {
            print("gyro not Available, please connect an iPhone or iPad")
        }
    }
    
    
    
    // MARK: - Accelerometer Record Section
    func verifBeforeStartAccelerometer(){
        if (isActiveSensorAccelerometer == false) {
            //print("Activation d'accelerometer")
            isActiveSensorAccelerometer = true
            startRecordAccelerometer()
        } else {
            //print("Activation de l'accelerometer déjà effectué")
        }
    }
    func startRecordAccelerometer() {
        if sensor.accelerometerAvailable {
            //print("accelerometer Available")
            sensor.accelerometerUpdateInterval = updateIntervalSensor
            let queue = NSOperationQueue.mainQueue()
            sensor.startAccelerometerUpdatesToQueue(queue) {
                (data, error) in
                
                print("Accelerometer || \nx = \(Double(data!.acceleration.x))\ny = \(Double(data!.acceleration.y))\nz = \(Double(data!.acceleration.z)) \n")
                self.arrAllDataAcce.append([
                    data!.acceleration.x,
                    data!.acceleration.y,
                    data!.acceleration.z
                ])

            }
        } else {
            print(" accelerometer not Available please connect an iPhone or iPad")
        }
    }

}

























