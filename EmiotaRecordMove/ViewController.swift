//
//  ViewController.swift
//  EmiotaRecordMove
//
//  Created by Eddy Rogier on 24/05/2016.
//  Copyright © 2016 EddyRogier. All rights reserved.
//

import UIKit



class ViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var mTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("viewcontroller")
        mTableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        mTableView.reloadData()
    }
    // MARK: - UITableDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moveMgr.movesArr.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.mTableView.dequeueReusableCellWithIdentifier("MoveCell", forIndexPath: indexPath) as! MoveCell
        
        //\/  set custom label
        //\/  image movement
        cell.moveImage.image = moveMgr.movesArr[indexPath.row].image
        cell.moveImage.highlightedImage = moveMgr.movesArr[indexPath.row].image
        
        if ( moveMgr.movesArr[indexPath.row].objectifCurrent == moveMgr.movesArr[indexPath.row].objectif) {
            cell.moveImageDone.image = UIImage.init(named: "check")
            cell.moveImageDone.highlightedImage = UIImage.init(named: "check")
        } else {
            cell.moveImageDone.image = UIImage.init(named: "uncheck")
            cell.moveImageDone.highlightedImage = UIImage.init(named: "uncheck")
        }
        cell.moveTitle.text = moveMgr.movesArr[indexPath.row].title
        cell.moveObjectif.text = String(moveMgr.movesArr[indexPath.row].objectif)
        cell.moveUnitObjectitif.text = moveMgr.movesArr[indexPath.row].isSecond ? "Left Second" : "Left Step"
        
        return cell
    }
    
    
    // MARK : - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let identifier = segue.identifier {
            switch identifier {
            case "Show details":
							
                /** ref DetailsVC */
                let refDVC = segue.destinationViewController as! DetailsViewController
                let senderCell = sender as! UITableViewCell
                refDVC.DVCindexPath = self.mTableView.indexPathForCell(senderCell)
            default:
                break
            }
        }
    }
}

